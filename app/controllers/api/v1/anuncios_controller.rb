class Api::V1::AnunciosController < Api::V1::ApiController
  before_action :authenticate_api_v1_user!, except: [:show]
  before_action :set_anuncio, only: [:show, :update, :destroy]
  before_action :allow_only_owner, only: [:update, :destroy]

  def index
    @anuncios = current_api_v1_user.anuncios
    render json: @anuncios.to_json
  end

  def show
    render json: @anuncio
  end

  def update
    @anuncio.update(anuncio_params)
    render json: @anuncio
  end

  def create
    @anuncio = Anuncio.create(anuncio_params)
    render json: @anuncio
  end

  def destroy
    @anuncio.destroy
    render json: {message: 'ok'}
  end

    private

      def set_anuncio
        @anuncio = Anuncio.friendly.find(params[:id])    
      end

      def allow_only_owner
        unless current_api_v1_user == @anuncio.user
          render(json: {}, status: :forbidden) and return
        end
      end

      def anuncio_params
        params.require(:anuncio).permit(:title, :description, :enable).merge(user: current_api_v1_user)        
      end
end