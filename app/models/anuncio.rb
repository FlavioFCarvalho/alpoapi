class Anuncio < ApplicationRecord
  belongs_to :user
  extend FriendlyId
  #todo title vai virar uma url
  friendly_id :title, use: :slugged
  validates :title, :description, :user, presence: true
  validates :title, uniqueness: true 
end
