class AddSlugToAnuncios < ActiveRecord::Migration[5.2]
  def change
    add_column :anuncios, :slug, :string
    add_index  :anuncios, :slug, unique: true
  end
end
