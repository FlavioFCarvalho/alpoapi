class CreateAnuncios < ActiveRecord::Migration[5.2]
  def change
    create_table :anuncios do |t|
      t.string :title
      t.text :description
      t.references :user, foreign_key: true
      t.boolean :enable

      t.timestamps
    end
  end
end
