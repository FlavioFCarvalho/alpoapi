require 'rails_helper'

#index
describe "GET /anuncios" do
  context "With Invalid authentication headers" do
    it_behaves_like :deny_without_authorization, :get, "/api/v1/anuncios"
  end

  context "With Valid authentication headers" do
    before do
      @user = create(:user)
      @anuncio1 = create(:anuncio, user: @user)
      @anuncio2 = create(:anuncio, user: @user)

      get "/api/v1/anuncios", params: {}, headers: header_with_authentication(@user)
    end

    it "returns 200" do
      expect_status(200)
    end

    it "returns anuncio list with 2 anuncios" do
      expect(json.count).to eql(2)
    end

    it "returned anuncios have right datas" do
      expect(json[0]).to eql(JSON.parse(@anuncio1.to_json))
      expect(json[1]).to eql(JSON.parse(@anuncio2.to_json))
    end
  end
end

#Show
describe "GET /anuncios/:friendly_id" do
  before do
    @user = create(:user)
  end

  context "When anuncio exists" do

    context "And is enable" do
      before do
        @anuncio = create(:anuncio, user: @user, enable: true)
      end

      it "returns 200" do
        get "/api/v1/anuncios/#{@anuncio.friendly_id}", params: {}, headers: header_with_authentication(@user)
        expect_status(200)
      end

      it "returned anuncio with right datas" do
        get "/api/v1/anuncios/#{@anuncio.friendly_id}", params: {}, headers: header_with_authentication(@user)
        expect(json).to eql(JSON.parse(@anuncio.to_json))
      end
    end

    context "And is unable" do
      before do
        @anuncios = create(:anuncio, user: @user, enable: false)
      end

      it "returns 404" do
        get "/api/v1/anuncios/#{FFaker::Lorem.word}", params: {id: @anuncios.friendly_id}, headers: header_with_authentication(@user)
        expect_status(404)
      end
    end
  end

  context "When anuncios dont exists" do
    it "returns 404" do
      get "/api/v1/anuncios/#{FFaker::Lorem.word}", params: {}, headers: header_with_authentication(@user)
      expect_status(404)
    end
  end
end

#Create
describe "POST /anuncios" do
 
  context "With Invalid authentication headers" do
    it_behaves_like :deny_without_authorization, :post, "/api/v1/anuncios"
  end

  context "With valid authentication headers" do
    before do
      @user = create(:user)
    end

    context "And with valid params" do
      before do
        @anuncio_attributes = attributes_for(:anuncio)
        post "/api/v1/anuncios", params: {anuncio: @anuncio_attributes}, headers: header_with_authentication(@user)
      end

      it "returns 200" do
        expect_status(200)
      end

      it "anuncio are created with correct data" do
        @anuncio_attributes.each do |field|
          expect(Anuncio.first[field.first]).to eql(field.last)
        end
      end

      it "Returned data is correct" do
        @anuncio_attributes.each do |field|
          expect(json[field.first.to_s]).to eql(field.last)
        end
      end
    end

    context "And with invalid params" do
      before do
        @other_user = create(:user)
        post "/api/v1/anuncios", params: {anuncio: {}}, headers: header_with_authentication(@user)
      end

      it "returns 400" do
        expect_status(400)
      end
    end
  end
end


#Update

describe "PUT /anuncios/:friendly_id" do
 
  context "With Invalid authentication headers" do
    it_behaves_like :deny_without_authorization, :put, "/api/v1/anuncios/questionary"
  end

  context "With valid authentication headers" do
    before do
      @user = create(:user)
    end

    context "When anuncio exists" do

      context "And user is the owner" do
        before do
          @anuncio = create(:anuncio, user: @user)
          @anuncio_attributes = attributes_for(:anuncio, id: @anuncio.id)
          put "/api/v1/anuncios/#{@anuncio.friendly_id}", params: {anuncio: @anuncio_attributes}, headers: header_with_authentication(@user)
        end

        it "returns 200" do
          expect_status(200)
        end

        it "anuncio are updated with correct data" do
          @anuncio.reload
          @anuncio_attributes.each do |field|
            expect(@anuncio[field.first]).to eql(field.last)
          end
        end

        it "Returned data is correct" do
          @anuncio_attributes.each do |field|
            expect(json[field.first.to_s]).to eql(field.last)
          end
        end
      end

      context "And user is not the owner" do
        before do
          @anuncio = create(:anuncio)
          @anuncio_attributes = attributes_for(:anuncio, id: @anuncio.id)
          put "/api/v1/anuncios/#{@anuncio.friendly_id}", params: {anuncio: @anuncio_attributes}, headers: header_with_authentication(@user)
        end

        it "returns 403" do
          expect_status(403)
        end
      end
    end

    context "When anuncio dont exists" do
      before do
        @anuncio_attributes = attributes_for(:anuncio)
      end

      it "returns 404" do
        delete "/api/v1/anuncios/#{FFaker::Lorem.word}", params: {anuncio: @anuncio_attributes}, headers: header_with_authentication(@user)
        expect_status(404)
      end
    end
  end
end

#Delete

describe "DELETE /anuncios/:friendly_id" do
  before do
    @user = create(:user)
  end

  context "When anuncio exists" do

    context "And user is the owner" do
      before do
        @anuncio = create(:anuncio, user: @user)
        delete "/api/v1/anuncios/#{@anuncio.friendly_id}", params: {}, headers: header_with_authentication(@user)
      end

      it "returns 200" do
        expect_status(200)
      end

      it "anuncio are deleted" do
        expect(Anuncio.all.count).to eql(0)
      end
    end

    context "And user is not the owner" do
      before do
        @anuncio = create(:anuncio)
        delete "/api/v1/anuncios/#{@anuncio.friendly_id}", params: {}, headers: header_with_authentication(@user)
      end

      it "returns 403" do
        expect_status(403)
      end
    end
  end

  context "When anuncio dont exists" do
    it "returns 404" do
      delete "/api/v1/anuncios/#{FFaker::Lorem.word}", params: {}, headers: header_with_authentication(@user)
      expect_status(404)
    end
  end
end