FactoryBot.define do
  factory :anuncio do
    title { FFaker::Lorem.word }
    description { FFaker::Lorem.phrase }
    user
    enable { FFaker::Boolean.maybe }
  end
end


